// ==UserScript==
// @name        Remove Google redirects
// @namespace   google
// @description Remove redirects from Google Search result links.
// @include     https://www.google.*/*
// @version     1
// @grant       none
// ==/UserScript==

for (let element of document.querySelectorAll('#res .r > a')) {
    element.removeAttribute('onmousedown');
}
